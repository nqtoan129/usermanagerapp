//
//  UserData.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 11/01/2022.
//

import UIKit
import RealmSwift
import Alamofire

protocol UserDataProtocol: AnyObject {
    func getUserData(completion: @escaping  ((Result<[User], Error>) -> Void))
    func getUserDetailData(_ isGetLocal: Bool,login: String, completion: @escaping  ((Result<UserDetail, Error>) -> Void))
}

class UseData: BaseService, UserDataProtocol {
    static let share = UseData()
    
    func getUserDetailData(_ isUseLocal: Bool = true,login: String, completion: @escaping  ((Result<UserDetail, Error>) -> Void)) {
        if !isConnectedToInternet() , isUseLocal {
            guard let userDetail = AppDelegate.shared().realm.objects(UserDetail.self).filter({ $0.login == login}).first else {
                completion(.failure(ApiError.noStore))
                return
            }
            completion(.success(userDetail))
            return
        }
        GET(endpoint: endpoints.userDetail(login), params: [:]).responseJSON { response in
            switch response.result {
            case .success(let jsonData):
                guard let json = jsonData as? [String: Any],
                      let result: UserDetail = self.parseData(item: json) else {
                    completion(.failure(ApiError.parseError))
                    return
                }
                try! AppDelegate.shared().realm.write({
                    AppDelegate.shared().realm.add(result, update: .all)
                })
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getUserData(completion: @escaping ((Result<[User], Error>) -> Void)) {
        if !isConnectedToInternet() {
            let result: [User] = AppDelegate.shared().realm.objects(User.self).toAray()
            guard !result.isEmpty else {
                completion(.failure(ApiError.noStore))
                return
            }
            
            completion(.success(result))
            return
        }
        GET(endpoint: endpoints.userList, params: [:]).responseJSON { response in
            switch response.result {
            case .success(let json):
                guard let result: [User] = self.convertValues(JSON: json) else {
                    completion(.failure(ApiError.parseError))
                    return
                }
                try! AppDelegate.shared().realm.write({
                    AppDelegate.shared().realm.add(result, update: .all)
                })
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    fileprivate struct endpoints {
        static let userList                = "users"
        static func userDetail(_ login: String) -> String {
            return userList + "/\(login)"
        }
    }
}

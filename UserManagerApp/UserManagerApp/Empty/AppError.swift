//
//  AppError.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 13/01/2022.
//

import Foundation

enum ApiError: Error {
  case unauthorized       // 401
  case missingParameters  // 403
  case notFound           // 404
  case serverError        // 500
  case parseError
  case timeout
  case unknown
  case noStore
  case noItem
  case unauthorizedPassword
  case emailExist
  case emptyData
  case networkError
  case custom(message: String)
}

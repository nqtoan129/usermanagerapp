//
//  ReuseTableviewCell.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 11/01/2022.
//
import UIKit

open class ReusableTableViewCell: UITableViewCell {
    public class var reuseIdentifier: String {
        return "\(self.self)"
    }
    
    public static func registerWithTable(tableview: UITableView) {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: self.reuseIdentifier, bundle: bundle)
        tableview.register(nib, forCellReuseIdentifier: self.reuseIdentifier)
    }
}

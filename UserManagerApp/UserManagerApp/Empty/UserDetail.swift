//
//  UserDetail.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 13/01/2022.
//

import RealmSwift

@objcMembers class UserDetail: Object, Decodable {
    dynamic var login: String? = ""
    dynamic var avatarUrl: String? = ""
    dynamic var name: String? = "", company: String?
    dynamic var blog: String? = ""
    dynamic var location: String? = ""
    dynamic var email: String?
    dynamic var bio: String?
    dynamic var twitterUsername: String?
    dynamic var publicRepos = 0, publicGists = 0, followers = 0, following: Int = 0
    dynamic var createdAt, updatedAt: String?
    override class func primaryKey() -> String? {
        return "login"
    }
}

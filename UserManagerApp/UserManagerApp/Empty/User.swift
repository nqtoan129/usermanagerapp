//
//  User.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 11/01/2022.
//

import RealmSwift

// MARK: - Welcome10Element
@objcMembers class User: Object, Decodable {
    dynamic var login: String = ""
    dynamic var id: Int = -1
    dynamic var nodeId: String = ""
    dynamic var avatarUrl: String = ""
    dynamic var gravatarId: String = ""
    dynamic var url = ""
    dynamic var htmlUrl = ""
    dynamic var followersUrl: String = ""
    dynamic var followingUrl = ""
    dynamic var gistsUrl = ""
    dynamic var starredUrl: String = ""
    dynamic var subscriptionsUrl = ""
    dynamic var organizationsUrl = ""
    dynamic var reposUrl: String = ""
    dynamic var eventsUrl: String = ""
    dynamic var receivedEventsUrl: String = ""
    dynamic var siteAdmin: Bool = false
    
    override class func primaryKey() -> String? {
        return "login"
    }
}

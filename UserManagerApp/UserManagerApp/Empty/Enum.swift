//
//  Result.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 13/01/2022.
//

import Foundation

enum Result<T, Error: Swift.Error> {
    case success(T)
    case failure(Error)
  
  var error: Error? {
    switch self {
    case .failure(let error): return error
    default: return nil
    }
  }
}

enum StatsPublic: String {
    case publicRepos = "PUBLIC REPO", publicGists = "PUBLIC GIST", followers = "FOLLOWERS", following = "FOLLOWING"
    case none = ""
}

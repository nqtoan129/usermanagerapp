//
//  UserDetailVC.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 11/01/2022.
//

import UIKit
import Kingfisher
import ESPullToRefresh

class UserDetailVC: UIViewController {
    // MARK: - IBoutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var publicRepoLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    private var viewModel: UserDetailVM!
    
    // MARK: - Life cycle
    init?(coder: NSCoder, viewModel: UserDetailVM) {
        self.viewModel = viewModel
        super.init(coder: coder)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        setupUI()
        updateUI()
        viewModel.getUserDetail()
        scrollView.refreshIdentifier = "Your Identifier" // Set refresh identifier
        scrollView.expiredTimeInterval = 20.0
        scrollView.es.autoPullToRefresh()
        scrollView.es.addPullToRefresh { [weak self] in
            guard let self = self else { return }
            // update data
            self.viewModel.getUserDetail()
            self.scrollView.es.stopPullToRefresh()
        }
    }
    
    private func setupUI() {
        title = "Profile"
        iconImageView.layer.cornerRadius = 35
        iconImageView.clipsToBounds = true
    }
    
    private func updateUI() {
        guard let userDetail = viewModel.userDetail else { return }
        if let urlString = userDetail.avatarUrl,
            let url = URL(string: urlString) {
            iconImageView.kf.setImage(with: url)
        }
        nameLabel.text = userDetail.name
        locationLabel.text = userDetail.location
        bioLabel.text = userDetail.bio
        publicRepoLabel.text = "\(userDetail.publicRepos)"
        attributeStatsLabel(owner: publicRepoLabel, numberPublic: "\(userDetail.publicRepos)", status: .publicRepos)
        attributeStatsLabel(owner: followersLabel, numberPublic: "\(userDetail.followers)", status: .followers)
        attributeStatsLabel(owner: followingLabel, numberPublic: "\(userDetail.following)", status: .following)
    }
    
    private func attributeStatsLabel(owner: UILabel, numberPublic: String, status: StatsPublic) {
        let attribute = NSMutableAttributedString(string: numberPublic, attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
        attribute.append(NSAttributedString(string: "\n\(status.rawValue)",
                                            attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray]))
        owner.attributedText = attribute
    }
}

// MARK: - Extension UserDetailVC
extension UserDetailVC: UserDetailVMDelegate {
    func didGetUserDetail(error: Error?) {
        if error == nil {
            DispatchQueue.main.async {[weak self] in
                self?.updateUI()
            }
        } else {
            print("error get user detail: ", error.debugDescription)
        }
    }
}

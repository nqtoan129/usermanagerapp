//
//  UserDetailVM.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 12/01/2022.
//

import Foundation
protocol UserDetailVMDelegate: NSObjectProtocol {
    func didGetUserDetail(error: Error?)
}

class UserDetailVM {
    weak var delegate: UserDetailVMDelegate?
    private let login: String!
    var userDetail: UserDetail?
    init(login: String) {
        self.login = login
    }
    func getUserDetail() {
        UseData.share.getUserDetailData(login: login) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let userDetail):
                self.userDetail = userDetail
                self.delegate?.didGetUserDetail(error: nil)
            case .failure(let error):
                self.delegate?.didGetUserDetail(error: error)
            }
            
        }
    }
}

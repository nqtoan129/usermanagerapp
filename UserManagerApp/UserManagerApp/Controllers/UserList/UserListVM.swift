//
//  UserListVM.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 11/01/2022.
//

import Foundation
import AVFoundation

protocol UserListVMDelegate: NSObjectProtocol {
    func didGetUserList(error: Error?)
}

class UserListVM {
    var users: [User] = []
    weak var delegate: UserListVMDelegate?
    func getUseList() {
        UseData.share.getUserData {[weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let users):
                self.users = users
                self.loadUserDetail()
                self.delegate?.didGetUserList(error: nil)
            case .failure(let error):
                self.delegate?.didGetUserList(error: error)
            }
        }
    }
    
    private func loadUserDetail() {
        let groupQueue = DispatchGroup()
        let logins = users.compactMap({ $0.login })
        let concurenceQueue = DispatchQueue(label: "loadUserDetail",attributes: .concurrent)
        print("detail count", logins.count)
        concurenceQueue.async {
            logins.forEach { login in
                groupQueue.enter()
                UseData.share.getUserDetailData(false, login: login, completion: { result in
                    groupQueue.leave()
                })
            }
        }
        groupQueue.notify(queue: .main) {
            print("finish load local detail ")
        }
    }
}

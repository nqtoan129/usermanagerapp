//
//  UserListVC.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 11/01/2022.
//

import UIKit
import ESPullToRefresh

class UserListVC: UIViewController {
    // MARK: - IBOulets
    @IBOutlet private weak var tableview: UITableView!
    
    private var viewModel = UserListVM()
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "User List"
        viewModel.delegate = self
        viewModel.getUseList()
        tableview.delegate = self
        tableview.dataSource = self
        UserListTbvCell.registerWithTable(tableview: tableview)
        tableview.es.addPullToRefresh {
            [unowned self] in
            self.viewModel.getUseList()
            tableview.es.stopPullToRefresh()
        }
    }
}

extension UserListVC: UserListVMDelegate {
    func didGetUserList(error: Error?) {
        DispatchQueue.main.async {[weak self] in
            if error == nil {
                self?.tableview.reloadData()
            } else {
                print(error.debugDescription)
            }
        }
        }
}

extension UserListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableview.dequeueReusableCell(withIdentifier: UserListTbvCell.reuseIdentifier) as? UserListTbvCell else {
            return UITableViewCell()
        }
        cell.configCell(user: viewModel.users[indexPath.row])
        return cell
    }
}

extension UserListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !viewModel.users[indexPath.row].login.isEmpty else {
            return
        }
        let userDetailVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(identifier: "UserDetailVC", creator: { coder -> UserDetailVC? in
            UserDetailVC(coder: coder, viewModel: UserDetailVM(login: self.viewModel.users[indexPath.row].login))
        })
        navigationController?.pushViewController(userDetailVC, animated: true)
    }
}

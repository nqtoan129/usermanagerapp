//
//  UserListTbvCell.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 11/01/2022.
//

import UIKit
import Foundation
import Kingfisher

enum TypeEnum {
    case organization
    case user
}

class UserListTbvCell: ReusableTableViewCell {
    // MARK: - IBOutlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconImageView.layer.cornerRadius = 25
        iconImageView.clipsToBounds = true
        // Initialization code
    }

    func configCell(user: User) {
        guard let url = URL(string: user.avatarUrl) else {
            return
        }
        
        iconImageView.kf.setImage(with: url)
        titleLabel.text = user.login
        descriptionLabel.text = user.htmlUrl
    }
}

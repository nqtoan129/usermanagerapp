//
//  Results.swift
//  UserManagerApp
//
//  Created by Quốc Toàn Nguyễn on 12/01/2022.
//

import RealmSwift

extension Results {
    func toAray<T>() -> [T] {
        var arr = [T]()
        if self.count == 0 {
            return arr
        }
        self.forEach({ element in
            if let e = element as? T {
                arr.append(e)
            }
        })
        return arr
    }
}

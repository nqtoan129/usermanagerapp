import Foundation
import Alamofire
import RealmSwift

extension BaseService {
    internal func convertValues<Type: Decodable>(JSON: Any) -> [Type]? {
        guard let responseJSON = JSON as? [[String: Any]]else {
            print("convertValues failse")
            return nil
        }
        
        var results = [Type]()
        for item in responseJSON {
            let assignment: Type? = parseData(item: item)
            if let ass = assignment {
                results.append(ass)
            }
        }
        
        return results
    }
    
    internal func parseData<T: Decodable>(item: [String: Any]) -> T? {
        var user: T?
        let strJSONData = self.convertToJson(dict: item).data(using: .utf8)!
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        do {
            let userData = try decoder.decode(T.self, from: strJSONData)
            user = userData
        } catch let error {
            print(error)
            return nil
        }
        return user
    }
    
    internal func convertToJson<T>(dict: T) -> String {
        if let theJSONData = try? JSONSerialization.data(withJSONObject: dict, options: []) {
            let theJSONText = String(data: theJSONData, encoding: .utf8)
            return theJSONText!
        } else {
            return ""
        }
    }
    
    internal func isConnectedToInternet() ->Bool {
        if let network = NetworkReachabilityManager() {
            return network.isReachable
        }
        return false
    }
}

import Foundation
import Alamofire

typealias complitionSuccess<T> = (T) -> ()
typealias complitionEmptySuccess = () -> ()
typealias complitionFailed = complitionSuccess<String>

class BaseService {
    func GET(endpoint: String, params:[String: Any]) -> Alamofire.DataRequest {
        return sendRequest(.get, endpoint: endpoint, param: params)
    }
    
    func POST(endpoint: String, params: [String: Any], header: HTTPHeaders? = nil) -> Alamofire.DataRequest {
        sendRequest(.post, endpoint: endpoint, param: params, headers: header)
    }
    
    private func sendRequest(_ method: Alamofire.HTTPMethod, endpoint: String, param:[String: Any], headers: HTTPHeaders? = nil) -> Alamofire.DataRequest {
//        let header = HTTPHeaders()
        if method == .get {
            return AF.request(getEndpoint(endpoint), method: method, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (res) in
            })
        } else {
            return AF.request(getEndpoint(endpoint), method: method, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (res) in
                
            })
        }
    }
    
    private func getEndpoint(_ endpoint: String) -> String {
        print("url request", "https://api.github.com/\(endpoint)")
        return "https://api.github.com/\(endpoint)"
    }
}

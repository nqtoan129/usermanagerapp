//
//  UserManagerAppTests.swift
//  UserManagerAppTests
//
//  Created by Quốc Toàn Nguyễn on 13/01/2022.
//

import XCTest
@testable import UserManagerApp

class UserManagerAppTests: XCTestCase {
    
    func testUser() {
        UseData.share.getUserDetailData(login: "2") { result in
            switch result {
            case .success(let userDetail):
                XCTAssertEqual((userDetail.name), ("hello"))
                XCTAssertEqual(userDetail.following, 4)
            case .failure(let error):
                print("error = ", error)
            }
        }
    }
}
